
import java.util.Scanner;

public class OXProgram {

    static Scanner kb = new Scanner(System.in);

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row, col:");
        row = kb.nextInt();
        col = kb.nextInt();
        checkRowCol();
    }

    public static void checkRowCol() {
        if(row > 3 || row < 1 || col > 3 || col < 1){
            System.out.println("Please input number between 1-3");
            System.out.println();
            showTurn();
            inputRowCol();
        }
    }

    private static void process() {

        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            if (checkDraw()) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        } else {
            System.out.println("This posision has already been used. Pless try again.");
            System.out.println();
            showTurn();
            inputRowCol();
            process();
        }
    }

    public static void showWin() {
        showTable();
        System.out.println(">>>" + currentPlayer + " Win<<<");
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    private static boolean setTable() {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer;
            return true;
        }
        return false;
    }

    private static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {

        }
        return false;
    }

    private static boolean checkX1() { // 1,1 2,2 3,3
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() { // 1,3 2,2 3,1
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

    private static void showDraw() {
        showTable();
        System.out.println(">>>Draw<<<");
    }

}
